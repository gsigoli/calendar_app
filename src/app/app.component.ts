import { Component } from '@angular/core';
import { ValidationService } from './validation/validation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'calendarApp';
}
