import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, take } from 'rxjs/operators';
import { User } from './user';

export const TOKEN_NAME = 'authJwtToken';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    private readonly API = `${environment.API}`;

    constructor(private http: HttpClient) { }

    login(user: User) {
        return this.http.post<User>(`${this.API}login`, user).pipe(take(1));
    }

    register(user: User) {
        return this.http.post<User>(`${this.API}register`, user).pipe(take(1));
    }

    isTokenExpired(): boolean {
        const token = localStorage.getItem(TOKEN_NAME);
        if (!token) {
            return true;
        }
    }


}
