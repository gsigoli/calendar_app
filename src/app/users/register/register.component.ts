import { ValidationService } from './../../validation/validation.service';
import { Router } from '@angular/router';
import { UserService } from './../user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      id: [null],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]],
    }, {
      validator: [
        ValidationService.match('password', 'confirmPassword'),
      ]
    });
  }

  register() {
    this.submitted = true;
    if (this.registerForm.valid) {
      this.userService.register(this.registerForm.value).subscribe(
        () => {
          this.router.navigateByUrl('/login');
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  hasError(field) {
    return this.registerForm.controls[field].errors && this.registerForm.controls[field].touched;
  }
}
