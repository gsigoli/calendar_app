import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  loginFailed = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });

    localStorage.removeItem('authJwtToken');

  }

  login() {
    this.submitted = true;
    if (this.loginForm.valid) {
      const login = this.loginForm.value;
      this.userService.login(login).subscribe(
        (reply: any) => {
          localStorage.setItem('authJwtToken', reply.authJwtToken);
          this.router.navigateByUrl('/event');
        },
        error => {
          console.log(error);
          this.loginFailed = true;
        }
      );
    }
  }

  hasError(field) {
    return this.loginForm.controls[field].errors && this.loginForm.controls[field].touched;
  }
}
