import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './auth.interceptor';
import { AuthGuard } from './guards/auth.guard';

import { EventsModule } from './events/events.module';
import { UsersModule } from './users/users.module';
import { ValidationModule } from './validation/validation.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    ValidationModule,
    BrowserModule,
    AppRoutingModule,
    EventsModule,
    UsersModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
