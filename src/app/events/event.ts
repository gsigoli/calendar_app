export interface Event {
  _id: string;
  description: string;
  initialDate: string;
  finalDate: string;
  startTime: string;
  endTime: string;
  user_id: string;
}
