import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ValidationService } from './../../validation/validation.service';

import { EventService } from './../event.service';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';


@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  eventForm: FormGroup;
  submitted = false;
  delete = false;
  overwrite = false;

  constructor(
    private fb: FormBuilder,
    private service: EventService,
    private location: Location,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.eventForm = this.fb.group({
      _id: [null],
      description: [null, [Validators.required, Validators.maxLength(60)]],
      initialDate: [null, [Validators.required, ValidationService.date]],
      finalDate: [null, [Validators.required, ValidationService.date]],
      startTime: [null, [Validators.required, ValidationService.time]],
      endTime: [null, [Validators.required, ValidationService.time]],
      user_id: [null],
    }, {
      validator: [
        ValidationService.datePeriod('initialDate', 'finalDate'),
        ValidationService.timePeriod('startTime', 'endTime')
      ]
    });

    this.route.params.pipe(
      map((params: any) => params.id),
      switchMap(id => {
        if (id) {
          return this.service.loadByID(id);
        }
        throw null;
      })
    ).subscribe(
      event => this.setEvent(event),
      () => {}
      );

  }

  fillFinalDate() {
    if (this.eventForm.value.finalDate == null) {
      this.eventForm.patchValue({
        finalDate: this.eventForm.value.initialDate
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.eventForm.valid) {
      this.service.save(this.eventForm.value).subscribe(
        success => this.onCancel(),
        error => {
            this.overwrite = true;
        }
      );
    }
  }

  setEvent(event) {
    this.eventForm.patchValue({
      _id: event._id,
      description: event.description,
      initialDate: event.initialDate,
      finalDate: event.finalDate,
      startTime: event.startTime,
      endTime: event.endTime,
      user_id: event.user_id,
    });
  }

  onDelete() {
    this.service.delete(this.eventForm.value).subscribe(
      success => this.location.back(),
      error => console.log(error)
    );
  }

  onCancel() {
    this.location.back();
  }

  hasError(field) {
    return this.eventForm.controls[field].errors && this.eventForm.controls[field].touched;
  }

}
