import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, take } from 'rxjs/operators';

import { Event } from './event';


@Injectable({
  providedIn: 'root'
})
export class EventService {

  private readonly API = `${environment.API}event`;

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Event[]>(this.API).pipe(take(1));
  }

  save(event) {
    if (event._id != null) {
      return this.http.put(`${this.API}/${event._id}`, event).pipe(take(1));
    }
    return this.http.post(this.API, event).pipe(take(1));
  }

  loadByID(id) {
    return this.http.get(`${this.API}/${id}`).pipe(take(1));
  }

  delete(event) {
    return this.http.delete(`${this.API}/${event._id}`).pipe(take(1));
  }

}
