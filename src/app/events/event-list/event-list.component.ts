import { Component, OnInit } from '@angular/core';

import { EventService } from '../event.service';
import { Subject, Observable, empty } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Event } from '../event';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  error$ = new Subject<boolean>();
  events$ = new Observable<Event[]>();

  constructor(
    private eventService: EventService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.events$ = this.eventService.getAll()
      .pipe(
        catchError(error => {
          console.log('erro', error);
          this.error$.next(true);
          return empty();
        }),
      );
  }

  onEdit(id) {
    this.router.navigate(['edit', id], { relativeTo: this.route });
  }

}
