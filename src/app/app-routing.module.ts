import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventListComponent } from './events/event-list/event-list.component';
import { EventFormComponent } from './events/event-form/event-form.component';
import { LoginComponent } from './users/login/login.component';
import { RegisterComponent } from './users/register/register.component';
import { AuthGuard } from './guards/auth.guard'


const routes: Routes = [
  { path: 'event', component: EventListComponent, canActivate: [ AuthGuard ]},
  { path: 'event/new', component: EventFormComponent, canActivate: [ AuthGuard ]},
  { path: 'event/edit/:id', component: EventFormComponent, canActivate: [ AuthGuard ]},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: '**', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
