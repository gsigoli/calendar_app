import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { ErrorMessageComponent } from './error-message/error-message.component';

@NgModule({
  exports: [
    ErrorMessageComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
  ],
  declarations: [
    ErrorMessageComponent
  ]
})
export class ValidationModule { }
