import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Form } from '@angular/forms';
import * as moment from 'moment';

const D_FORMAT = 'DDMMYYYY';
const T_FORMAT = 'HHmm';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config = {
      required: 'Campo obrigatório',
      email: 'Email inválido',
      minlength: `O campo deve ter no mínimo ${validatorValue.requiredLength} caracteres`,
      maxlength: `O campo deve ter no máximo ${validatorValue.requiredLength} caracteres`,
      invalidMatch: 'As senhas devem ser iguais',
      invalidPeriod: 'Período Inválido',
      invalidDate: 'Data Inválida',
      invalidTime: 'Hora Inválida',
    };
    return config[validatorName];
  }

  static match(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.invalidMatch) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ invalidMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  static date(control: FormControl) {
    return moment(control.value, D_FORMAT, true).isValid() ? null : { invalidDate: true };
  }

  static datePeriod(initialName: string, finalName: string) {
    return (formGroup: FormGroup) => {
      const initialDate = formGroup.controls[initialName];
      const finalDate = formGroup.controls[finalName];

      if (initialDate.errors && !finalDate.errors) {
        return;
      }
      if (moment(initialDate.value, D_FORMAT).isBefore(moment(finalDate.value, D_FORMAT)) ||
        initialDate.value === finalDate.value) {
        finalDate.setErrors(null);
      } else {
        finalDate.setErrors({ invalidPeriod: true });
      }
    };
  }

  static time(control: FormControl) {
    return moment(control.value, T_FORMAT, true).isValid() ? null : { invalidTime: true };
  }

  static timePeriod(startTimeName: string, endTimeName: string) {
    return (formGroup: FormGroup) => {
      const startTime = formGroup.controls[startTimeName];
      const endTime = formGroup.controls[endTimeName];

      if (startTime.errors && !endTime.errors) {
        return;
      }
      if (moment(startTime.value, T_FORMAT).isBefore(moment(endTime.value, T_FORMAT)) || startTime.value === endTime.value) {
        endTime.setErrors(null);
      } else {
        endTime.setErrors({ invalidPeriod: true });
      }
    };
  }
}
